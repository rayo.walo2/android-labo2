package com.example.labo2;

import java.util.ArrayList;


public class Cart {
    public int totalQty = 0;
    public double totalPrice = 0;
    private ArrayList<Product> cartlist = new ArrayList<Product>();
    public Cart()
    {
        totalQty = 0;
        totalPrice = 0;
    }

    public void add(Product product,int qte)
    {
        boolean exist = false;
        for (Product p:cartlist) {
            if(p.getname()==product.getname() && p.getimg()==product.getimg())
            {
                exist=true;
            }
        }
        if (exist==false) {
            product.setqte(qte);
            cartlist.add(product);
            totalQty+=product.getqte();
            totalPrice+=product.getprice()*product.getqte();
        } else {
            changeqte(product,product.getqte()+qte);
        }

    }

    public double getTaxValue()
    {
        return totalPrice*(0.15);
    }
    public double getTotalPrice()
    {
        double total=totalPrice+getTaxValue();
        return total;
    }
    public void changeqte(Product product,int qte)
    {
        for (Product p:cartlist) {
            if(p==product)
            {
                int qty=qte;
                int TmpQ= qty - p.getqte();
                p.setqte(qty);
                totalPrice+=p.getprice()*TmpQ;
                totalQty+=TmpQ;

            }
        }
    }

    public void remove(Product product)
    {
        cartlist.remove(product);
        totalQty-=product.getqte();
        totalPrice-=product.getprice()*product.getqte();
    }
    public ArrayList<Product> getCartList()
    {
        return cartlist;
    }
}
