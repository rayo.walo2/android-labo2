package com.example.labo2;

public class Product {
    private String name;
    private String quantity;
    private String img;
    private double price;
    private int qte;


    public Product(String name,String quantity, String img, double price){


        this.name=name;
        this.quantity=quantity;
        this.img=img;
        this.price=price;
        this.qte=0;
    }


    public String getname() {
        return this.name;
    }
    public String getquantity() {
        return this.quantity;
    }
    public String getimg() {
        return this.img;
    }
    public double getprice() {
        return this.price;
    }
    public int getqte() { return this.qte; }
    public void setqte(int qte) { this.qte=qte; }
}
