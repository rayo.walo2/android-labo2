package com.example.labo2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Document;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
public class Products extends AppCompatActivity {
    private TextView nomCatView;
    private ListView listView1;
    private ArrayAdapter<String> itemsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        Intent intent = getIntent();
        String nomCat = intent.getExtras().getString("nomCat");
        this.nomCatView=findViewById(R.id.nomCatView);
        this.nomCatView.setText(nomCat);

        dbWorker db= new dbWorker(this,nomCat);
        db.execute();
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_options_menu, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                startActivity(new Intent(this, MainActivity.class));
                return true;
            case R.id.cart:
                startActivity(new Intent(this, CartActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public class dbWorker extends AsyncTask
    {
        private Context c;
        private String nomCat;
        public dbWorker(Context c,String nomCat)
        {
            this.c=c;
            this.nomCat=nomCat;
        }
        @Override
        protected void onPreExecute() {
        }
        @Override
        protected Object doInBackground(Object[] objects) {
            URL url;
            ArrayList<Product> products=new ArrayList<>();

            try {

                Document doc = Jsoup.connect("https://www.metro.ca/en/online-grocery/aisles/"+nomCat).get();
                Elements links = doc.select(".products-tile-list__tile");
                for(Element el : links) {


                    String name=el.select(".tile-product__top-section .pt-title").text();
                    String image= el.select(".tile-product__top-section .tile-product__top-section__visuals a img").attr("src");
                    String quantity=el.select(".tile-product__top-section .pt-weight").text();
                    double price=Double.valueOf(el.select(".pi--main-price").attr("data-main-price"));
                    Product p = new Product(name,quantity,image,price);
                    products.add(p);
                }
                return products;
            }
            catch (IOException e) {
                   return e.getMessage();
            }
        }
        @Override
        protected void onPostExecute(Object o)
        {
            final ArrayList<Product> products =(ArrayList<Product>)o;
            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(linearLayoutManager);
            ProductsCustomAdapter customAdapter = new ProductsCustomAdapter(Products.this, products);
            recyclerView.setAdapter(customAdapter);
        }

    }
}

