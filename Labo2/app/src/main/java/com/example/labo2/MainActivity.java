package com.example.labo2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Spinner;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.view.View;
import android.content.Intent;
public class MainActivity extends AppCompatActivity {
    private ListView listView1;
    private List<String> cat;
    private ArrayAdapter<String> itemsAdapter;
    static final Cart cart = new Cart();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        List<String> catsList = Arrays.asList( "fruits-vegetables", "dairy-eggs", "pantry", "beverages", "beer-wine", "meat-poultry","vegan-vegetarian-food","organic-groceries","snacks","frozen","bread-bakery-products","deli-prepared-meals","fish-seafood","world-cuisine","household-cleaning","baby","health-beauty","pet-care","pharmacy","nature-s-signaure");
        final ArrayList<String> l = new ArrayList<>();
        l.addAll(catsList);
        listView1 = findViewById(R.id.listView1);
        itemsAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1,l);
        listView1.setAdapter(itemsAdapter);
        listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
                String nomCat = l.get(position);
                Intent i = new Intent(MainActivity.this, Products.class);
                i.putExtra("nomCat", nomCat);
                startActivity(i);
            }
        });
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_options_menu, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                startActivity(new Intent(this, MainActivity.class));
                return true;
            case R.id.cart:
                startActivity(new Intent(this, CartActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
