package com.example.labo2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;

import static com.example.labo2.MainActivity.cart;

public class CartActivity extends AppCompatActivity {
    private TextView subtotal,taxes,total,qtytotal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        CartActivity.dbWorker db= new CartActivity.dbWorker(this);
        db.execute();
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_options_menu, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                startActivity(new Intent(this, MainActivity.class));
                return true;
            case R.id.cart:
                startActivity(new Intent(this, CartActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public class dbWorker extends AsyncTask
    {
        private Context c;
        public dbWorker(Context c)
        {
            this.c=c;
        }
        @Override
        protected void onPreExecute() {
        }
        @Override
        protected Object doInBackground(Object[] objects) {
            URL url;
            ArrayList<Product> products= cart.getCartList();
            return products;
        }
        @Override
        protected void onPostExecute(Object o)
        {
            final ArrayList<Product> products =(ArrayList<Product>)o;
            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerViewCart);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(linearLayoutManager);
            CartCustomAdapter customAdapter = new CartCustomAdapter(CartActivity.this, products);
            recyclerView.setAdapter(customAdapter);
            DecimalFormat df = new DecimalFormat("0.00");
            subtotal=findViewById(R.id.subtotal);
            subtotal.setText("Sub Total : $"+df.format(cart.totalPrice));
            taxes=findViewById(R.id.taxes);
            taxes.setText("Taxes : $"+df.format(cart.getTaxValue()));
            total=findViewById(R.id.total);
            total.setText("Total: $"+df.format(cart.getTotalPrice()));
            qtytotal=findViewById(R.id.qtytotal);
            qtytotal.setText("Total quantity: "+ cart.totalQty);
        }

    }
}

