package com.example.labo2;

import android.app.AlertDialog;
import android.content.Context;

import java.util.ArrayList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import static com.example.labo2.MainActivity.cart;

public class ProductsCustomAdapter extends RecyclerView.Adapter<ProductsCustomAdapter.MyViewHolder> {

    ArrayList<Product> products;
    Context context;

    public ProductsCustomAdapter(Context context, ArrayList<Product> products) {
        this.context = context;
        this.products = products;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardlayout, parent, false);
        MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
        return vh;
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.name.setText(products.get(position).getname());
        ImageView image = holder.image;
        String imageUrl = products.get(position).getimg();
        Picasso.get().load(imageUrl).into(image);
        holder.price.setText(products.get(position).getprice()+" $");
        holder.quantity.setText(products.get(position).getquantity());
        final NumberPicker np = holder.qte;
        np.setMinValue(1);
        np.setMaxValue(20);
        np.setOnValueChangedListener(onValueChangeListener);
        holder.add_to_cart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                cart.add(products.get(position),np.getValue());
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Product Added!!");
                builder.setMessage(np.getValue()+" item(s) of the following product was/were added successfully: "+products.get(position).getname());
                builder.setPositiveButton("Done", null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        // implement setOnClickListener event on item view.
    }
    NumberPicker.OnValueChangeListener onValueChangeListener = new 	NumberPicker.OnValueChangeListener(){
        @Override
        public void onValueChange(NumberPicker numberPicker, int i, int i1) {
            Toast.makeText(context,
                    "selected number "+numberPicker.getValue(), Toast.LENGTH_SHORT);
        }
    };

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView price;
        TextView quantity;
        NumberPicker qte;
        ImageView image;
        Button add_to_cart;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            image = (ImageView) itemView.findViewById(R.id.image);
            price = (TextView) itemView.findViewById(R.id.price);
            quantity = (TextView) itemView.findViewById(R.id.quantity);
            qte = (NumberPicker) itemView.findViewById(R.id.qte);
            add_to_cart = (Button) itemView.findViewById(R.id.add_to_cart);

        }
    }
}