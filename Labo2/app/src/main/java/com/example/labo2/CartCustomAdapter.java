package com.example.labo2;

import android.content.Context;

import java.util.ArrayList;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import static com.example.labo2.MainActivity.cart;

public class CartCustomAdapter extends RecyclerView.Adapter<CartCustomAdapter.MyViewHolder> {

    ArrayList<Product> products;
    Context context;

    public CartCustomAdapter(Context context, ArrayList<Product> products) {
        this.context = context;
        this.products = products;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_items_layout, parent, false);
        MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
        return vh;
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.name.setText(products.get(position).getname());
        ImageView image = holder.image;
        String imageUrl = products.get(position).getimg();
        Picasso.get().load(imageUrl).into(image);
        holder.price.setText(products.get(position).getprice()+" $");
        holder.quantity.setText(products.get(position).getquantity());
        final NumberPicker np = holder.qte;
        np.setMinValue(1);
        np.setMaxValue(20);
        np.setOnValueChangedListener(onValueChangeListener);
        np.setValue(products.get(position).getqte());
        holder.remove_from_cart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                cart.remove(products.get(position));
                Intent intent = new Intent(context, CartActivity.class);
                context.startActivity(intent);
            }
        });
        holder.change_qty_cart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                cart.changeqte(products.get(position),np.getValue());
                Intent intent = new Intent(context, CartActivity.class);
                context.startActivity(intent);
            }
        });
    }
    NumberPicker.OnValueChangeListener onValueChangeListener = new 	NumberPicker.OnValueChangeListener(){
        @Override
        public void onValueChange(NumberPicker numberPicker, int i, int i1) {
            Toast.makeText(context,
                    "selected number "+numberPicker.getValue(), Toast.LENGTH_SHORT);
        }
    };

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView price;
        TextView quantity;
        NumberPicker qte;
        ImageView image;
        Button remove_from_cart;
        Button change_qty_cart;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            image = (ImageView) itemView.findViewById(R.id.image);
            price = (TextView) itemView.findViewById(R.id.price);
            quantity = (TextView) itemView.findViewById(R.id.quantity);
            qte = (NumberPicker) itemView.findViewById(R.id.qte);
            remove_from_cart = (Button) itemView.findViewById(R.id.remove_from_cart);
            change_qty_cart = (Button) itemView.findViewById(R.id.change_qty_cart);

        }
    }
}